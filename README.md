# alpaka-group-container

This repository contains container recipes for [Alpaka](https://github.com/alpaka-group/alpaka) and [Cupla](https://github.com/alpaka-group/cupla) for CI testing and providing development environments.

# install

```bash
pip install -r generator/requirements.txt
```

# usage

```bash
python generator/recipe.py [--gcc] [--clang] [--cuda {8,9.0,9.1,9.2,10.0,10.1,10.2}] > Dockerfile
docker build -t alpaka-ci:latest .
```

# structure

* `generator/recipe.py` is a frontend for the terminal and handles terminal arguments to generate different container recipes.
* `generator/generate.py` contains the actual code for recipe generation. The script contains different layers that can be stacked to a complete recipe.
* `generator/dev_cuda.py` generates a recipe for a container to develop an alpaka application with `CUDA`. The container contains only a single version of `Boost` and `CUDA` and install all packages into the default paths (e.g. `/usr/local`).
* `generator/ci_picongpu.py` is a special script for the CI. It allows to split the build step of the base container and the build step of the PIConGPU extension into two different CI jobs without building the base image twice.
