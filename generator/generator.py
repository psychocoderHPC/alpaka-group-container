# Copyright 2020 Simeon Ehrig
#
# ISC Software License


from typing import Dict, List, KeysView
from distutils.version import StrictVersion

import hpccm
from hpccm.primitives import baseimage, shell, environment, comment, user
from hpccm.building_blocks.boost import boost
from hpccm.building_blocks.cmake import cmake
from hpccm.building_blocks.gnu import gnu
from hpccm.building_blocks.llvm import llvm
from hpccm.building_blocks.packages import packages

from hpccm.templates.git import git
from hpccm.templates.CMakeBuild import CMakeBuild
from hpccm.templates.rm import rm
from hpccm.templates.downloader import downloader
from hpccm.templates.ConfigureMake import ConfigureMake

# binary download
cmake_version: str = "3.18.3"

# build from source
boost_versions: List[str] = [
    "1.65.1",
    "1.66.0",
    "1.67.0",
    "1.68.0",
    "1.69.0",
    "1.70.0",
    "1.71.0",
    "1.72.0",
    "1.73.0",
    "1.74.0",
]

# installed via apt
gcc_versions: List[str] = ["5", "6", "7", "8", "9", "10"]

# installed via apt
clang_versions: List[str] = ["4.0", "5.0", "6.0", "7", "8", "9", "10", "11"]

# cuda base image and extra installation via .deb package
# the cuda-develop container contains patched gcc versions, which are not available on
# normal systems
# base, runtime and devel have to replaced by a placeholder {0}
cuda_versions: Dict[float, Dict[str, str]] = {
    8: {
        "image": "nvidia/cuda:8.0-{0}-ubuntu16.04",
        "deb-name": "cuda-repo-ubuntu1604-8-0-local",
        "file-name": "cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64-deb",
        "url": "https://developer.nvidia.com/compute/cuda/8.0/Prod2/local_installers/cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64-deb",
    },
    9.0: {
        "image": "nvidia/cuda:9.0-{0}-ubuntu16.04",
        "deb-name": "cuda-repo-ubuntu1604-9-0-local",
        "file-name": "cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64-deb",
        "url": "https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64-deb",
    },
    9.1: {
        "image": "nvidia/cuda:9.1-{0}-ubuntu16.04",
        "deb-name": "cuda-repo-ubuntu1604-9-1-local",
        "file-name": "cuda-repo-ubuntu1604-9-1-local_9.1.85-1_amd64",
        "url": "https://developer.nvidia.com/compute/cuda/9.1/Prod/local_installers/cuda-repo-ubuntu1604-9-1-local_9.1.85-1_amd64",
    },
    9.2: {
        "image": "nvidia/cuda:9.2-{0}-ubuntu16.04",
        "deb-name": "cuda-repo-ubuntu1604-9-2-local",
        "file-name": "cuda-repo-ubuntu1604-9-2-local_9.2.88-1_amd64",
        "url": "https://developer.nvidia.com/compute/cuda/9.2/Prod/local_installers/cuda-repo-ubuntu1604-9-2-local_9.2.88-1_amd64",
    },
    10.0: {
        "image": "nvidia/cuda:10.0-{0}-ubuntu18.04",
        "deb-name": "cuda-repo-ubuntu1804-10-0-local",
        "file-name": "cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64",
        "url": "https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64",
    },
    10.1: {
        "image": "nvidia/cuda:10.1-{0}-ubuntu18.04",
        "deb-name": "cuda-repo-ubuntu1804-10-1-local",
        "file-name": "cuda-repo-ubuntu1804-10-1-local-10.1.168-418.67_1.0-1_amd64.deb",
        "url": "https://developer.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda-repo-ubuntu1804-10-1-local-10.1.168-418.67_1.0-1_amd64.deb",
    },
    10.2: {
        "image": "nvidia/cuda:10.2-{0}-ubuntu18.04",
        "deb-name": "cuda-repo-ubuntu1804-10-2-local",
        "file-name": "cuda-repo-ubuntu1804-10-2-local-10.2.89-440.33.01_1.0-1_amd64.deb",
        "url": "http://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda-repo-ubuntu1804-10-2-local-10.2.89-440.33.01_1.0-1_amd64.deb",
    },
    11.0: {
        "image": "nvidia/cuda:11.0-{0}-ubuntu18.04",
        "deb-name": "cuda-repo-ubuntu1804-11-0-local",
        "file-name": "cuda-repo-ubuntu1804-11-0-local_11.0.2-450.51.05-1_amd64.deb",
        "url": "http://developer.download.nvidia.com/compute/cuda/11.0.2/local_installers/cuda-repo-ubuntu1804-11-0-local_11.0.2-450.51.05-1_amd64.deb",
    },
}

# rocm base images
rocm_versions: Dict[float, Dict[str, str]] = {
    3.8: {
        "image": "rocm/dev-ubuntu-18.04:3.8",
        "distro": "ubuntu18",
    },
}


class recipe_generator:
    def __init__(
        self,
        used_boost_versions=boost_versions,
        gcc: bool = False,
        clang: bool = False,
        cuda_version: float = None,
        rocm_version: float = None,
        picongpu: bool = False,
    ):
        """Setup generator object for full stack build

        :param used_boost_versions: List of boost versions, which should
                                    installed
        :type used_boost_versions: List[str]
        :param gcc: Install extra gcc versions beside the system compiler.
        :type gcc: bool
        :param clang: Install extra clang version
        :type clang: bool
        :param cuda_version: Setup CUDA base image and install CUDA sdk.
        :type cuda_version: float
        :param rocm_version: Setup rocm base image.
        :type rocm_version: float
        :param picongpu: Build all dependencies of PIConGPU.
        :type picongpu: bool

        """

        if cuda_version != None and rocm_version != None:
            raise ValueError(
                "cuda_version and rocm_version cannot set at the same time"
            )

        self.used_boost_versions = used_boost_versions
        self.gcc = gcc
        self.clang = clang
        self.cuda_version = cuda_version
        self.rocm_version = rocm_version
        self.picongpu = picongpu

    def get_full_stack(self) -> hpccm.Stage:
        """Create full container stack with different container stages.

        :returns: Return hpccm Stage object
        :rtype: hpccm.Stage

        """
        stage = self.get_base_layer(
            cuda_version=self.cuda_version,
            cuda_from_source=True,
            rocm_version=self.rocm_version,
        )

        for instr in self.get_boost_layer(self.used_boost_versions):
            stage += instr

        if self.gcc:
            for instr in self.get_gcc_layer(gcc_versions):
                stage += instr

        if self.clang:
            for instr in self.get_clang_layer(clang_versions):
                stage += instr

        if self.picongpu:
            for instr in self.get_picongpu_layer():
                stage += instr

        return stage

    @staticmethod
    def get_base_layer(
        cuda_version: float = None,
        cuda_from_source: bool = False,
        rocm_version: float = None,
    ) -> hpccm.Stage:
        """Setup baseimage, install packages via apt and install cmake.

        :param cuda_version: If CUDA version is specified, use CUDA baseimage instead Ubuntu 18.04 image.
        :type cuda_version: float
        :param cuda_from_source: If true, install CUDA SDK via deb package, otherwise use CUDA-develop docker image. The source install contains just packages, which are neccesary for CI tests.
        :type: bool
        :param rocm_version: If rocm version is specified, use rocm baseimage instead Ubuntu 18.04 image. Important: The user is changed from "rocm-user" to "root" and must be changed back manually.
        :type: float
        :returns: return stage object
        :rtype: hpccm.Stage

        """
        if cuda_version:
            stage = recipe_generator.__get_cuda_base_image_from_source(
                cuda_version, cuda_from_source
            )
            distro = ""
        elif rocm_version:
            stage = hpccm.Stage()
            distro = rocm_versions[rocm_version]["distro"]
            stage.baseimage(image=rocm_versions[rocm_version]["image"], _distro=distro)
            stage += user(user="root")
        else:
            stage = hpccm.Stage()
            distro = "ubuntu18"
            stage.baseimage(image="ubuntu:bionic", _distro=distro)

        apt_package_list = [
            "bzip2",
            "libbz2-dev",
            "gcc",
            "g++",
            "git",
            "libc6-dev",
            "libomp-dev",
            "make",
            "rsync",
            "software-properties-common",
            "tar",
            "wget",
            "zlib1g-dev",
        ]

        if distro == "ubuntu18":
            # this apt package was renamed in Ubuntu 18.04
            # if the baseimage is a non-CUDA baseimage, we can be sure, that it is Ubuntu 18.04
            apt_package_list.append("gpg-agent")

        stage += packages(ospackages=apt_package_list)

        stage += cmake(eula=True, version=cmake_version)

        # install rocRand, because it is required by alpaka for the Hip back-end
        if rocm_version:
            stage += environment(variables={"PATH": "$PATH:/opt/rocm/bin"})
            stage += recipe_generator.__get_rocRand(rocm_version)

        return stage

    @staticmethod
    def __get_cuda_base_image_from_source(
        version: float, cuda_from_source: bool
    ) -> hpccm.Stage:
        """Returns a base image with CUDA SDK

        :param version: CUDA SDK version
        :type: float
        :param cuda_from_source: If true, install CUDA SDK via deb package, otherwise use CUDA-develop docker image. The source install contains just packages, which are neccesary for CI tests.
        :type: bool
        :returns: return stage object
        :rtype: hpccm.Stage

        """
        stage = hpccm.Stage()
        if not cuda_from_source:
            stage.baseimage(image=cuda_versions[version]["image"].format("devel"))
            return stage
        else:
            if version <= 8.0:
                stage.baseimage(image=cuda_versions[version]["image"].format("runtime"))
            else:
                stage.baseimage(image=cuda_versions[version]["image"].format("base"))

        apt_package_list = [
            "dirmngr",
            "wget",
        ]
        if "16.04" in cuda_versions[version]["image"]:
            apt_package_list.append("gnupg-agent")
        else:
            apt_package_list.append("gpg-agent")
        stage += packages(ospackages=apt_package_list)

        shell_commands = []
        shell_commands.append(
            "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F60F4B3D7FA2AF80"
        )
        shell_commands.append(
            "wget --no-verbose -O "
            + cuda_versions[version]["file-name"]
            + " "
            + cuda_versions[version]["url"]
        )
        shell_commands.append("dpkg -i ./" + cuda_versions[version]["file-name"])
        shell_commands.append("apt -y --quiet update")

        if version < 11.0:
            cuda_apt_packages = "cuda-core-{0} cuda-cudart-{0} cuda-cudart-dev-{0} cuda-curand-{0} cuda-curand-dev-{0}"
        else:
            cuda_apt_packages = "cuda-compiler-{0} cuda-cudart-{0} cuda-cudart-dev-{0} libcurand-{0} libcurand-dev-{0}"

        shell_commands.append(
            (
                "apt -y --quiet --allow-unauthenticated --no-install-recommends install "
                + cuda_apt_packages
            ).format(str(version))
        )
        shell_commands.append(
            "ln -s /usr/local/cuda-{0} /usr/local/cuda".format(str(version))
        )
        shell_commands.append("rm -rf " + cuda_versions[version]["file-name"])
        shell_commands.append("dpkg --purge " + cuda_versions[version]["deb-name"])
        shell_commands.append("rm -rf /var/lib/apt/lists/*")

        stage += shell(commands=shell_commands)

        # the path is necessary, that cmake finds the CUDA driver API
        stage += environment(
            variables={"LIBRARY_PATH": "/usr/local/cuda/lib64/stubs:$LIBRARY_PATH"}
        )

        return stage

    @staticmethod
    def __get_rocRand(version: float) -> hpccm.primitives.shell:
        """Install rocRand in existing rocm installation.

        :param version: version of rocm
        :type version: float
        :returns: a hpccm shell object with git and cmake instructions
        :rtype: hpccm.primitives.shell

        """
        cm = []

        rocRand_git = git()
        cm.append(
            rocRand_git.clone_step(
                repository="https://github.com/ROCmSoftwarePlatform/rocRAND.git",
                branch="rocm-{0}.0".format(str(version)),
                path="/opt/repos",
            )
        )

        rocRand_cmake = CMakeBuild(prefix="/opt/rocm")
        cm.append(
            rocRand_cmake.configure_step(
                directory="/opt/repos/rocRAND",
                opts=["-DCMAKE_CXX_COMPILER=/opt/rocm/bin/hipcc"],
            )
        )
        cm.append(rocRand_cmake.build_step(target="install"))

        return shell(commands=cm)

    @staticmethod
    def get_boost_layer(
        versions: List[str],
        install_prefix: str = "/opt/boost",
        toolset_compiler: str = "gcc",
    ):
        """Install boost from source.

        :param versions: List of the version to be installed.
        :type versions: List[str]
        :param install_prefix: Boost is installed to <install_prefix>/include. If more than one version is selected, the install path is <install_prefix>/<version>/include.
        :type install_prefix: str
        :param toolset_compiler: Set boost bootstrap option "--tool-set". Supported are gcc and clang.
        :type toolset_compiler: str
        :returns: List of hpccm objects that can be attached to the stage
        :rtype:

        """

        if toolset_compiler != "gcc" and toolset_compiler != "clang":
            raise ValueError("unknown boost toolset compiler: " + toolset_compiler)

        boost_install_instr = [
            comment("#######################"),
            comment("##### boost layer #####"),
            comment("#######################"),
        ]

        boost_install_instr.append(
            shell(commands=["mkdir -p /var/tmp", "mkdir -p /var/tmp/logs"])
        )

        for v in versions:
            boost_install = boost(
                prefix=install_prefix + "/" + v,
                version=v,
                environment=False,
                bootstrap_opts=[
                    "--with-libraries=atomic,chrono,context,date_time,fiber,filesystem,math,program_options,regex,serialization,system,thread"
                ],
                b2_opts=['cxxflags="-std=c++14"'],
            )

            # write build output of ./b2 to log file
            boost_str = ""
            for line in str(boost_install).split("\n"):
                if "./b2" in line:
                    idx = line.index("&& \\")
                    line = (
                        line[:idx]
                        + " 2>&1 > /var/tmp/logs/boost_{0}.log ".format(v)
                        + line[idx:]
                    )
                boost_str += line + "\n"

            boost_install_instr.append(shell(commands=[boost_str]))

        boost_install_instr.append(shell(commands=["rm -r /var/tmp/logs/boost_*"]))

        return boost_install_instr

    @staticmethod
    def get_gcc_layer(versions: List[str]):
        """Install gcc via apt.

        :param versions: List of the version to be installed.
        :type versions: List[str]
        :returns: List of hpccm objects that can be attached to the stage
        :rtype:

        """

        gcc_install_instr = [
            comment("#######################"),
            comment("###### gcc layer ######"),
            comment("#######################"),
        ]

        for v in versions:
            # GCC 10 is not available in the Ubuntu 16.04 packages
            if hpccm.config.g_linux_version == StrictVersion("16.04") and int(v) > 9:
                continue
            else:
                gcc_install_instr.append(
                    gnu(
                        version=v,
                        extra_repository=True,  # install from ppa and not source
                    )
                )

        return gcc_install_instr

    @staticmethod
    def get_clang_layer(versions: List[str]):
        """Install clang via apt.

        :param versions: List of the version to be installed.
        :type versions: List[str]
        :returns: List of hpccm objects that can be attached to the stage
        :rtype:

        """
        llvm_install_instr = [
            comment("#######################"),
            comment("##### clang layer #####"),
            comment("#######################"),
        ]

        if hpccm.config.g_linux_version == StrictVersion("16.04"):
            ubuntu_name = "xenial"
            # WORKAROUND: Clang 7 and 9 is not available in Xenial PPAs (6 and 8 are available)
            if "7" in versions:
                versions.remove("7")
            if "9" in versions:
                versions.remove("9")
        if hpccm.config.g_linux_version == StrictVersion("18.04"):
            ubuntu_name = "bionic"

        llvm_install_instr.append(
            shell(
                commands=[
                    "touch /etc/apt/sources.list.d/llvm.list",
                    "wget http://llvm.org/apt/llvm-snapshot.gpg.key",
                    "apt-key add llvm-snapshot.gpg.key",
                    "rm llvm-snapshot.gpg.key",
                    'echo "deb http://apt.llvm.org/{0}/ llvm-toolchain-{0} main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                    'echo "deb-src http://apt.llvm.org/{0}/ llvm-toolchain-{0} main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                    'echo "" >> /etc/apt/sources.list',
                    'echo "deb http://apt.llvm.org/{0}/ llvm-toolchain-{0}-10 main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                    'echo "deb-src http://apt.llvm.org/{0}/ llvm-toolchain-{0}-10 main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                    'echo "" >> /etc/apt/sources.list',
                    'echo "deb http://apt.llvm.org/{0}/ llvm-toolchain-{0}-11 main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                    'echo "deb-src http://apt.llvm.org/{0}/ llvm-toolchain-{0}-11 main" >> /etc/apt/sources.list.d/llvm.list'.format(
                        ubuntu_name
                    ),
                ]
            )
        )

        for v in versions:
            llvm_install_instr.append(
                llvm(
                    version=v,
                    extra_repository=True,  # install from ppa and not source
                )
            )

            # FIXME Workaround: the llvm OpenMP libraries from version 7 to current are interdependent and
            # cannot be installed in parallel
            # Hopefully it will be fixed in a future version
            if float(v) > 6.0:
                llvm_install_instr.append(
                    shell(
                        commands=["apt remove -y libomp-{0}-dev libomp5-{0}".format(v)]
                    )
                )

        # because of the workaround we have to install OpenMP again afterwards
        llvm_install_instr.append(packages(ospackages=["libomp-dev"]))

        # set latest clang compiler as default
        llvm_install_instr.append(
            shell(
                commands=[
                    "update-alternatives --install /usr/bin/clang clang $(which clang-{0}) 50".format(
                        versions[-1]
                    ),
                    "update-alternatives --install /usr/bin/clang++ clang++ $(which clang++-{0}) 50".format(
                        versions[-1]
                    ),
                    # remove llvm PPA after installation because they are
                    # not stable and often cause server timeouts
                    "rm /etc/apt/sources.list.d/llvm.list",
                ]
            )
        )

        return llvm_install_instr

    @staticmethod
    def get_picongpu_layer():
        """Build all dependencies of PIConGPU.

        :returns: List of hpccm objects that can be attached to the stage
        :rtype:

        """

        ev = env_helper()

        picongpu_install_instr = [
            comment("#######################"),
            comment("#### picongpu layer ###"),
            comment("#######################"),
        ]

        picongpu_install_instr.append(
            packages(
                ospackages=[
                    # for pngwriter
                    "libpng-dev",
                    # mpi general
                    "libopenmpi-dev",
                    "openmpi-bin",
                    "openssh-server",
                    # for HDF5
                    "zlib1g-dev",
                ]
            )
        )

        #######################
        ###### pngwirter ######
        #######################
        picongpu_install_instr.append(comment("## install pngwriter ##"))

        cm = []

        pngwriter_git = git()
        cm.append(
            pngwriter_git.clone_step(
                repository="https://github.com/pngwriter/pngwriter.git",
                branch="0.7.0",
                path="/opt/repos",
            )
        )

        pngwriter_cmake = CMakeBuild(prefix="/opt/pngwriter/0.7.0")
        cm.append(pngwriter_cmake.configure_step(directory="/opt/repos/pngwriter"))
        cm.append(pngwriter_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/pngwriter/0.7.0")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ######## HDF5 #########
        #######################
        picongpu_install_instr.append(comment("## install HDF5 ##"))

        cm = ev.get_export_cmd()

        hdf5_downloader = downloader(
            url="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/hdf5-1.8.20/src/hdf5-1.8.20.tar.gz"
        )

        cm.append(hdf5_downloader.download_step(unpack=True, wd="/opt/repos"))

        hdf5_configureMake = ConfigureMake(prefix="/opt/hdf5/1.8.20")
        cm.append(
            hdf5_configureMake.configure_step(
                directory="/opt/repos/hdf5-1.8.20",
                build_directory="/opt/repos/hdf5-1.8.20/build",
                opts=["--enable-parallel", "--enable-shared"],
            )
        )
        cm.append(hdf5_configureMake.build_step())
        cm.append(hdf5_configureMake.install_step())

        ev.add(name="HDF5_ROOT", val="/opt/hdf5/1.8.20")
        ev.append(name="LD_LIBRARY_PATH", val="/opt/hdf5/1.8.20/lib")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ###### libsplash ######
        #######################
        picongpu_install_instr.append(comment("## install libsplash ##"))

        cm = ev.get_export_cmd()
        cm.append("export USER=root")

        libsplash_git = git()
        cm.append(
            libsplash_git.clone_step(
                repository="https://github.com/ComputationalRadiationPhysics/libSplash.git",
                branch="v1.7.0",
                path="/opt/repos",
            )
        )

        libsplash_cmake = CMakeBuild(prefix="/opt/libsplash/1.7.0")
        cm.append(
            libsplash_cmake.configure_step(
                directory="/opt/repos/libSplash",
                opts=["-DSplash_USE_MPI=ON", "-DSplash_USE_PARALLEL=ON"],
            )
        )
        cm.append(libsplash_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/libsplash/1.7.0")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ####### ADIOS 1 #######
        #######################
        picongpu_install_instr.append(comment("## install ADIOS 1 ##"))

        cm = ev.get_export_cmd()
        cm += ["export CC=mpicc", "export CXX=mpicxx"]

        adios1_git = git()
        cm.append(
            adios1_git.clone_step(
                repository="https://github.com/ornladios/ADIOS.git",
                directory="ADIOS1",
                branch="v1.13.1",
                path="/opt/repos",
            )
        )

        adios1_cmake = CMakeBuild(prefix="/opt/adios/1.13.1")
        cm.append(
            adios1_cmake.configure_step(
                directory="/opt/repos/ADIOS1",
                opts=[
                    "-DBUILD_FORTRAN=OFF",
                    "-DBUILD_ZFP=OFF",
                    "-DZFP=OFF",
                    "-DCMAKE_CXX_FLAGS=-fPIC",
                    "-DCMAKE_C_FLAGS=-fPIC",
                ],
            )
        )
        cm.append(adios1_cmake.build_step(target="install", parallel="1"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/adios/1.13.1")
        ev.append(name="PATH", val="/opt/adios/1.13.1/bin")
        cm += ["unset CC", "unset CXX"]
        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ####### ADIOS 2 #######
        #######################
        picongpu_install_instr.append(comment("## install ADIOS 2 ##"))

        cm = ev.get_export_cmd()
        cm += ["export CC=mpicc", "export CXX=mpicxx"]

        adios2_git = git()
        cm.append(
            adios2_git.clone_step(
                repository="https://github.com/ornladios/ADIOS2",
                branch="v2.6.0",
                path="/opt/repos",
            )
        )

        adios2_cmake = CMakeBuild(prefix="/opt/adios/2.6.0")
        cm.append(
            adios2_cmake.configure_step(
                directory="/opt/repos/ADIOS2",
                opts=[
                    "-DADIOS2_BUILD_EXAMPLES=OFF",
                    "-DADIOS2_BUILD_TESTING=OFF",
                    "-DADIOS2_USE_Fortran=OFF",
                    "-DCMAKE_CXX_FLAGS=-fPIC",
                    "-DCMAKE_C_FLAGS=-fPIC",
                ],
            )
        )
        cm.append(adios2_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/adios/2.6.0")
        ev.append(name="PATH", val="/opt/adios/2.6.0/bin")
        cm += ["unset CC", "unset CXX"]
        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ####### jansson #######
        #######################
        picongpu_install_instr.append(comment("## install jansson ##"))

        cm = ev.get_export_cmd()

        jansson_git = git()
        cm.append(
            jansson_git.clone_step(
                repository="https://github.com/akheron/jansson.git",
                branch="v2.9",
                path="/opt/repos",
            )
        )

        jansson_cmake = CMakeBuild(prefix="/opt/jansson/2.9.0")
        cm.append(
            jansson_cmake.configure_step(
                directory="/opt/repos/jansson",
            )
        )
        cm.append(jansson_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/jansson/2.9.0/lib/cmake")
        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ######## icet #########
        #######################
        picongpu_install_instr.append(comment("## install icet ##"))

        cm = ev.get_export_cmd()

        icet_git = git()
        cm.append(
            icet_git.clone_step(
                repository="https://gitlab.kitware.com/paraview/icet.git",
                branch="IceT-2.1.1",
                path="/opt/repos",
            )
        )

        icet_cmake = CMakeBuild(prefix="/opt/icet/2.9.0")
        cm.append(
            icet_cmake.configure_step(
                directory="/opt/repos/icet",
                opts=["-DICET_USE_OPENGL=OFF", "-DBUILD_TESTING=OFF"],
            )
        )
        cm.append(icet_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/icet/2.9.0")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ######## isaac ########
        #######################
        picongpu_install_instr.append(comment("## install isaac ##"))

        cm = ev.get_export_cmd()

        isaac_git = git()
        cm.append(
            isaac_git.clone_step(
                repository="https://github.com/ComputationalRadiationPhysics/isaac.git",
                branch="v1.5.0",
                path="/opt/repos",
            )
        )

        isaac_cmake = CMakeBuild(prefix="/opt/isaac/1.5.0")
        cm.append(
            isaac_cmake.configure_step(
                directory="/opt/repos/isaac/lib",
            )
        )
        cm.append(isaac_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/isaac/1.5.0")

        picongpu_install_instr.append(shell(commands=cm))

        cm = ev.get_export_cmd()

        isaac_dev_git = git()
        cm.append(
            isaac_dev_git.clone_step(
                repository="https://github.com/ComputationalRadiationPhysics/isaac.git",
                branch="dev",
                path="/opt/repos",
                directory="issac-dev",
            )
        )

        isaac_dev_cmake = CMakeBuild(prefix="/opt/isaac/1.6.0-dev")
        cm.append(
            isaac_dev_cmake.configure_step(
                directory="/opt/repos/issac-dev/lib",
            )
        )
        cm.append(isaac_dev_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/isaac/1.6.0-dev")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ##### openpmd-api #####
        #######################
        picongpu_install_instr.append(comment("## install openpmd-api ADIOS 2 ##"))

        cm = ev.get_export_cmd()

        openpmd_git = git()
        cm.append(
            openpmd_git.clone_step(
                repository="https://github.com/openPMD/openPMD-api.git",
                commit="0.12.0-alpha",
                path="/opt/repos",
            )
        )

        openpmd_cmake = CMakeBuild(prefix="/opt/openPMD-api/0.12.0")
        cm.append(
            openpmd_cmake.configure_step(
                directory="/opt/repos/openPMD-api",
                opts=[
                    "-DopenPMD_USE_ADIOS2=ON",
                    "-DopenPMD_USE_ADIOS1=OFF",
                    "-DopenPMD_USE_PYTHON=OFF",
                ],
            )
        )
        cm.append(openpmd_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/openPMD-api/0.12.0")

        picongpu_install_instr.append(shell(commands=cm))

        # deprecated: build with ADIOS 1
        #######################
        ##### openpmd-api #####
        #######################
        picongpu_install_instr.append(comment("## install openpmd-api with ADIOS 1 ##"))

        cm = ev.get_export_cmd()

        cm += [
            "cd /opt/repos/openPMD-api",
            "git checkout 5f0f5849985c1054b7eff1f8e6922ec22de36e9d",
            "cd /",
        ]

        openpmd1_cmake = CMakeBuild(prefix="/opt/openPMD-api/0.12.0-dev")
        cm.append(
            openpmd1_cmake.configure_step(
                directory="/opt/repos/openPMD-api",
                opts=[
                    "-DopenPMD_USE_ADIOS2=OFF",
                    "-DopenPMD_USE_ADIOS1=ON",
                    "-DopenPMD_USE_PYTHON=OFF",
                ],
            )
        )
        cm.append(openpmd1_cmake.build_step(target="install"))

        ev.append(name="CMAKE_PREFIX_PATH", val="/opt/openPMD-api/0.12.0-dev")

        picongpu_install_instr.append(shell(commands=cm))

        #######################
        ###### clean up #######
        #######################

        # remove repos and build folder
        r = rm()
        picongpu_install_instr.append(
            shell(commands=[r.cleanup_step(items=["/opt/repos/"])])
        )

        return picongpu_install_instr


class env_helper:
    def __init__(self):
        """Small helper class that creates 'export' bash commands for each stage (the export command in a Docker RUN command is only valid within the RUN command)"""
        self.single_env: Dict[str, str] = {}
        self.env_vars: Dict[str, List[str]] = {}

    def add(self, name: str, val: str):
        """Set export name=val. Overwrite existing variables

        :param name: name of the variable
        :type name: str
        :param val: value of the variable
        :type val: str

        """
        self.single_env[name] = val

    def append(self, name: str, val: str):
        """Set export name=val:$name . If a variable already exists, append the variable.

        :param name: name of the variable
        :type name: str
        :param val: value of the variable
        :type val: str

        """
        if name in self.env_vars:
            self.env_vars[name].insert(0, val)
        else:
            self.env_vars[name] = [val]

    def get_export_cmd(self) -> List[str]:
        """Returns all bash 'export' commands.

        :returns: list of bash 'export' commands
        :rtype: List[str]

        """
        cmds: List[str] = []
        for k in self.single_env:
            cmds.append("export {0}={1}".format(k, self.single_env[k]))

        for k in self.env_vars:
            s = "export " + k + "="
            for v in self.env_vars[k]:
                s += v + ":"
            s += "$" + k
            cmds.append(s)
        return cmds
