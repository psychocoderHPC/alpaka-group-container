# Copyright 2020 Simeon Ehrig
#
# ISC Software License


import argparse

import generator as gn


def parseArgs() -> argparse.Namespace:
    """Parse the input arguments.

    :returns: argparse.Namespace object
    :rtype: argparse.Namespace

    """
    parser = argparse.ArgumentParser(
        description="Generator of Dockerfiles for Alpka and Cupla.",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--boost",
        type=str,
        nargs="+",
        help="Install specific boost versions.\n"
        "Versions: " + ", ".join(gn.boost_versions),
    )

    parser.add_argument(
        "--gcc",
        action="store_true",
        help="Install gcc compiler beside the system compiler.\n"
        "Versions: " + ", ".join(gn.gcc_versions),
    )

    parser.add_argument(
        "--clang",
        action="store_true",
        help="Install clang compiler.\n" "Versions: " + ", ".join(gn.clang_versions),
    )

    parser.add_argument(
        "--cuda",
        type=float,
        help="Select CUDA base image and SDK version. If not set, use Ubuntu 18.04.",
        choices=gn.cuda_versions.keys(),
    )

    parser.add_argument(
        "--rocm",
        type=float,
        help="Select rocm base image and SDK version. If not set, use Ubuntu 18.04.",
        choices=gn.rocm_versions.keys(),
    )

    parser.add_argument(
        "--picongpu",
        action="store_true",
        help="Install all requirements for PIConGPU.",
    )

    return parser.parse_args()


def main():
    args = parseArgs()

    if args.boost:
        for b in args.boost:
            if b not in gn.boost_versions:
                print(b + " is not a supported boost version")
                exit(1)
        boost_version = args.boost
    else:
        boost_version = gn.boost_versions

    rg = gn.recipe_generator(
        used_boost_versions=boost_version,
        gcc=args.gcc,
        clang=args.clang,
        cuda_version=args.cuda,
        rocm_version=args.rocm,
        picongpu=args.picongpu,
    )

    print(rg.get_full_stack())


if __name__ == "__main__":
    main()
