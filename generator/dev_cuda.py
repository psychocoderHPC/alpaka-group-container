# Copyright 2020 Simeon Ehrig
#
# ISC Software License

import sys
import generator as gn


def main():
    """Generates a recipe for a container designed for the development of Alpaka applications with the CUDA backend.

    """
    cuda_version = 10.0
    boost_version = "1.73.0"

    rg = gn.recipe_generator
    stage = rg.get_base_layer(cuda_version, cuda_from_source=False)
    stage += rg.get_boost_layer(versions=[boost_version], install_prefix="/usr/local")

    print(stage)


if __name__ == "__main__":
    main()
